import examples from './examples';

export default {
  examples,
  bootstrapComponent: 'b-badge',
  bootstrapPropsInfo: {
    variant: {
      enum: 'variantOptions',
    },
  },
};
